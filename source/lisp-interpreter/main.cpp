//
//  main.cpp
//
//  lis-interpreter    :   an interpreter for the lis programming language.
//
//  Created by Daniel Rehman on 2002182.
//  Copyright © 2020 Daniel Rehman. All rights reserved.
//
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <cctype>
#include <cmath>
#include <readline/readline.h>
#include <readline/history.h>

using number = double long;

struct list {
    std::string identifier = "";
    std::vector<struct list> symbols = {};
    bool error = false;
};

struct entry {
    std::string name = "";
    number value = 0;
};

static inline void replace_all(std::string& string, const std::string& find, const std::string& replace) {
    size_t pos = string.find(find);
    while (pos != std::string::npos) {
        string.replace(pos, find.size(), replace);
        pos = string.find(find, pos + replace.size());
    }
}
 
static inline std::vector<std::string> lex(std::string text) {
    replace_all(text, "(", " ( ");
    replace_all(text, ")", " ) ");
    std::istringstream stringstream(text);
    std::vector<std::string> result(std::istream_iterator<std::string>(stringstream),{});
    return result;
}

static inline list parse(std::vector<std::string> tokens, size_t& index) {
    auto t = tokens.at(index++);
    if (t != "(" and t != ")") return {t};
    else if (t == "(") {
        list l = {};
        while (index < tokens.size() and tokens.at(index) != ")") {
            auto p = parse(tokens, index);
            l.symbols.push_back(p);
        }
        index++;
        return l;
    } else return {"", {}, true};
}

void print_lex(const std::vector<std::string>& result) {
    std::cout << "lex: [ ";
    for (auto r : result) {
        std::cout << "\"" << r << "\", ";
    }
    std::cout << "]\n";
}

void print_list_line(list l) {
    if (l.identifier != "") std::cout << l.identifier;
    else {
        std::cout << "(";
        for (int i = 0; i < l.symbols.size(); i++) {
            print_list_line(l.symbols.at(i));
            if (i != l.symbols.size() - 1) std::cout << " ";
        }
        std::cout << ")";
    }
}

static inline number lookup(std::string name, std::vector<entry> table) {
    for (auto e : table) if (e.name == name) return e.value;
    std::cout << "error: unidentified variable: " << name << "\n";
    return 0;
}

static inline number evaluate(list e, std::vector<entry>& table) {
    if (e.identifier.length() and isdigit(e.identifier.front())) return std::stoi(e.identifier);
    else if (e.identifier.length()) return lookup(e.identifier, table);
    else if (e.symbols.empty()) return 0; /// ()
    const auto op = e.symbols[0].identifier;
    
    if (op == "comment") return 0;
    
    else if (op == "+") { /// (+ 1 2 3 4 5)
        number sum = 0;
        for (int i = 1; i < e.symbols.size(); i++) sum += evaluate(e.symbols[i], table);
        return sum;
        
    } else if (op == "++") { /// (++ x)
        for (auto& t : table) if (t.name == e.symbols[1].identifier) return ++t.value;
        
    } else if (op == "--") { /// (-- x)
        for (auto& t : table) if (t.name == e.symbols[1].identifier) return --t.value;
        
    } else if (op == "*") { /// (* 1 2 3 4 5)
        number product = 1;
        for (int i = 1; i < e.symbols.size(); i++) product *= evaluate(e.symbols[i], table);
        return product;
        
    } else if (op == "set") { /// (set a 4)
        if (e.symbols.size() != 3) std::cout << "error: set: incorrect number of arguments\n";
        else {
            for (auto& t : table) if (t.name == e.symbols[1].identifier) return t.value = evaluate(e.symbols[2], table);
            std::cout << "error: unidentified variable: " << e.symbols[1].identifier << "\n";
        }
    
    } else if (op == "=") { /// (= 1 2 6 6 7)
        bool equal = true;
        if (e.symbols.size()) {
            auto first = evaluate(e.symbols[1], table);
            for (int i = 2; i < e.symbols.size(); i++)
                if (first != evaluate(e.symbols[i], table)) equal = false;
        }
        return equal;
    
    } else if (op == "not" ) { /// (not 0)      ---> 1
        if (e.symbols.size() != 2) std::cout << "error: not: incorrect number of arguments\n";
        else return not evaluate(e.symbols[1], table);
    
    } else if (op == "<" ) { /// (< 1 2)        ---> 1
        if (e.symbols.size() != 3) std::cout << "error: <: incorrect number of arguments\n";
        else return evaluate(e.symbols[1], table) < evaluate(e.symbols[2], table);
        
    } else if (op == ">" ) { /// (> 1 2)        ---> 0
        if (e.symbols.size() != 3) std::cout << "error: >: incorrect number of arguments\n";
        else return evaluate(e.symbols[1], table) > evaluate(e.symbols[2], table);
        
    } else if (op == "-") { /// (- 1 2)        ---> -1
        if (e.symbols.size() != 3) std::cout << "error: -: incorrect number of arguments\n";
        else return evaluate(e.symbols[1], table) - evaluate(e.symbols[2], table);
                        
    } else if (op == "/") { /// (/ 1 2)         --->  0.5
        if (e.symbols.size() != 3) std::cout << "error: /: incorrect number of arguments\n";
        else {
            auto d = evaluate(e.symbols[2], table);
            if (!d) { std::cout << "error: arithematic division by 0\n"; }
            else return evaluate(e.symbols[1], table) / d;
        }
    } else if (op == "%" ) { /// (% 1 2)         --->  1
        if (e.symbols.size() != 3) std::cout << "error: %: incorrect number of arguments\n";
        else {
            auto m = evaluate(e.symbols[2], table);
            if (!m) { std::cout << "error: arithematic modulus by 0\n"; }
            else return fmodl(evaluate(e.symbols[1], table), m);
        }
    } else if (op == "let") {
        if (e.symbols.size() != 3) std::cout << "error: let: incorrect number of arguments\n";
        else {
            auto value = evaluate(e.symbols[2], table);
            table.push_back({e.symbols[1].identifier, value});
            return value;
        }
    } else if (op == "if") {  /// (if cond then else)
        if (e.symbols.size() != 4) std::cout << "error: if: incorrect number of arguments\n";
        else if (evaluate(e.symbols[1], table)) return evaluate(e.symbols[2], table);
        else return evaluate(e.symbols[3], table);
        
    } else if (op == "while") {  /// (while cond statement)
        if (e.symbols.size() != 3) std::cout << "error: while: incorrect number of arguments\n";
        else while (evaluate(e.symbols[1], table)) evaluate(e.symbols[2], table);
         
    } else if (op == "printn") {  /// (printn 65 66 67)           ---> 65 66 67
        for (int i = 1; i < e.symbols.size(); i++) {
            printf("%.Lf", evaluate(e.symbols[i], table));
            if (i != e.symbols.size() - 1) std::cout << " ";
        }
    } else if (op == "print") {  /// (print hello there from space)           ---> hello there from space\n
        for (int i = 1; i < e.symbols.size(); i++) {
            std::cout << e.symbols[i].identifier << " ";
        } std::cout << "\n";
    
    } else if (op == "prints") {  /// (print hello there from space)           ---> hello there from space\n
        for (int i = 1; i < e.symbols.size(); i++) {
            std::cout << e.symbols[i].identifier;
            if (i != e.symbols.size() - 1) std::cout << " ";
        }
        
    } else if (op == "printc") { /// (putc 65 66 67)          ---> ABC
        for (int i = 1; i < e.symbols.size(); i++) std::cout << (char) evaluate(e.symbols[i], table);
    
    } else if (op == "char") { /// (char A)          ---> 65
        if (e.symbols.size() != 2) std::cout << "error: char: incorrect number of arguments\n";
        else return e.symbols[1].identifier.front();
    
    } else if (op == "read") {
        if (e.symbols.size() != 2) std::cout << "error: read: incorrect number of arguments\n";
        else {
            for (auto& t : table) {
                if (t.name == e.symbols[1].identifier) {
                    std::cin >> t.value;
                    return t.value;
                }
            }
            std::cout << "error: unidentified variable: " << e.symbols[1].identifier << "\n";
        }
    } else std::cout << "error: operation \""<< op <<"\" not bound\n";
    return 0;
}

static inline void print_table(std::vector<entry> table) {
    std::cout << "symbol table: \n";
    for (auto e : table)
        std::cout << "\t" << e.name << "\t:\t" << e.value << "\n";
    std::cout << "\n";
}

static inline number interpret(const std::string &text, std::vector<entry>& table) {
    size_t index = 0;
    return evaluate(parse(lex(text), index), table);
}

static inline void print_commands() {
    std::cout << "commands:\n";
    std::cout << "\tquit(q) - quit the repl.\n";
    std::cout << "\thelp - this menu.\n";
    std::cout << "\tclear(l) - clear the screen.\n";
    std::cout << "\tshow - print current state of the symbol table.\n";
    std::cout << "\toperations - print builtin operations in the language.\n";
    std::cout << "\t(expression) - evaluates the expr and prints the result.\n\n";
}

static inline void repl(std::vector<entry>& table) {
    std::cout << "REPL for my lis interpreter.\ntype \"help\" for more info.\n\n";
    while (true) {
        char* line = readline(" : ");
        add_history(line);
        if (!strcmp(line, "")) {}
        else if (!strcmp(line, "quit") or !strcmp(line, "q")) { free(line); break; }
        else if (!strcmp(line, "help")) print_commands();
        else if (!strcmp(line, "show")) print_table(table);
        else if (!strcmp(line, "clear") or !strcmp(line, "l")) printf("\033[1;1H\033[2J");
        else if (!strcmp(line, "operations")) std::cout <<
            "builtin operations: "
            "comment, +, -, /, *, %, <, >, =, "
            "if, while, not, let, set, read, "
            "printc, prints, printn, print, char.\n";
        else {
            auto result = interpret(line, table);
            std::cout << " = " << result << "\n";
        }
        free(line);
    }
    clear_history();
}

int main(int argc, const char * argv[]) {
    
    std::vector<entry> table = {
        {"true", 1},
        {"false", 0},
        
        {"space", 32},
        {"newline", 10},
        
        {"null", 0},
        {"dummy", 1234567890},
    };
    
    if (argc == 1) {
        repl(table);
        exit(0);
        
    } else for (int i = 1; i < argc; i++) {
        std::ifstream file {argv[i]};
        if (not file.good()) std::cout << "error: could not open file: " << argv[i] << "\n";
        interpret({std::istreambuf_iterator<char>(file), {}}, table);
    }
}
